
export function AppController(heroService, videoService) {

  let ctrl = this;

  heroService.getHero()
  .then(function(hero) {
    return ctrl.hero = hero;
  })

  videoService.getVideos()
  .then(function(videos) {
    return ctrl.videos = videos;
  });

}
