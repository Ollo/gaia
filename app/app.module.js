
import { AppController } from './app-ctrl';
import { videoService } from './components/video-cards/video-service';
import { heroService } from './components/hero/hero-service';
import { components } from './components/components.module';

angular.module('app', ['components'])
  .constant('COMPONENTS', '/app/components/')
  .constant('API', 'http://www.gaia.com/api/videos/term/119931')
  .controller('AppController', AppController)
  .service('videoService', videoService)
  .service('heroService', heroService)
