import { loader } from './loader/loader';
import { siteHeader } from './site-header/site-header';
import { videoCard } from './video-cards/video-cards';
import { hero } from './hero/hero';

angular.module('components', [])
  .directive('loader', loader)
  .directive('siteHeader', siteHeader)
  .directive('videoCard', videoCard)
  .directive('hero', hero)
