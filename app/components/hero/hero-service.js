

export function heroService($http, API) {

  let self = this;

  self.getHero = function getHero() {
    return $http.get(API)
    .then(function(resp) {
      return resp.data.term;
    })
  }

}
