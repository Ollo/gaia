
export function hero(COMPONENTS, heroService) {
  return {
    restrict: 'E',
    templateUrl: COMPONENTS + 'hero/hero.html',
    replace: true
  }
}
