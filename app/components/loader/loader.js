export function loader() {
  return {
    restrict: 'E',
    template: '<div class="loader"><img ng-src="/build/images/loader.gif" /></div>',
    replace: true
  }
}
