
export function videoCard(COMPONENTS) {
  return {
    restrict: 'E',
    templateUrl: COMPONENTS + 'video-cards/video-card.html',
    replace: true,
    scope: {
      video: '='
    }
  }
}
