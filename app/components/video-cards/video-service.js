

export function videoService($http, API) {

  let self = this;

  self.getVideos = function getVideos() {
    return $http.get(API)
    .then(function(resp) {
      return resp.data.titles;
    })
  }

}
