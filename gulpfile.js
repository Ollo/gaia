var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var livereload = require('gulp-livereload');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var webpack = require('gulp-webpack');

gulp.task('sass', function() {
  return gulp.src('app/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('build/css'))
    .pipe(livereload());
});

gulp.task('bundle', function() {
  return gulp.src('app/app.module.js')
    .pipe(sourcemaps.init())
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('build/js'))
    .pipe(livereload());
})

gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/**/*.js', ['bundle']);
  gulp.watch('app/**/*.html', ['bundle']);

  function notifyLiveReload(event) {
    gulp.src(event.path, {read: false})
      .pipe(livereload());
  }

});
