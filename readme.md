# gaia

**a sample app to demonstrate**

-  project structure
-  promises
-  using the API
-  componentized structure
-  basic page layout, and styling

## Notes:

**js**

I tried a different build approach for this app from my previous projects.
I went with es6, babel and then bundled via webpack.
This was mainly to have some fun but to also an attempt to improve
the readability of some of the more tedious angular syntax.
I've defined a few components to show some general structure but there were
several more in the comp if you would like further examples.

**styles**

I used my personal sass boilerplate and added Susy. I wanted to experiment with Susy as I haven't used it in the past and typically have used Google Material or Bourbon Neat. I also pulled in font awesome via a cdn for mocking but would plan to optimize / customize icons for production.

## Development

set up project

```
npm install
```

run the dev server

```
npm run
```

open your browser to `localhost:8080`

### building
```
gulp watch // watch for sass js and html changes
gulp sass // compile sass
gulp bundle // bundle js via webpack
```
