var webpack = require('webpack');
var path = require('path');

const PATHS = {
  app: __dirname + '/app',
  dist: __dirname + '/build'
}

module.exports = {
  context: PATHS.app,
  entry: {
    app: './app.module.js'
  },
  output: {
    path: PATHS.dist,
    filename: "app.bundle.js"
  },
  module: {
    loaders: [
      { test: /\.scss$/, loader: "style!css!sass" },
      { test: /\.css$/, loader: "style!css" },
      { test: /\.js$/, exclude: /node_modules/, loader: "babel", query: {presets:['es2015']}},
      { test: /\.json$/, loader: 'json' },
    ]
  },
  plugins: []
};
